use_gpu = false; % gpu usage flag.
ktype = 'Gaussian'; % kernel type.
floatx = @(x) single(x);

% Load MNIST dataset.
mnist_path = 'mnist.mat';
data = load(mnist_path);

if use_gpu
    train_x = gpuArray(floatx(data.train_x));
    train_y = gpuArray(floatx(data.train_y));
    test_x = gpuArray(floatx(data.test_x));
    test_y = gpuArray(floatx(data.test_y));
    new_weight = @(n, d) gpuArray(floatx(zeros(n, d)));
else
    train_x = floatx(data.train_x);
    train_y = floatx(data.train_y);
    n = 3000;
    train_x = train_x(1:n,:);
    train_y = train_y(1:n,:);
    test_x = floatx(data.test_x);
    test_y = floatx(data.test_y);
    new_weight = @(n, d) floatx(zeros(n, d));
end

% Parameters.
M = 4000;           % (EigenPro) subsample size.
k = 160;            % (EigenPro) top-k eigensystem.
mG = 12;
% Set kernel bandwidth.
if strcmp(ktype, 'Gaussian')
	s = 5;
elseif strcmp(ktype, 'Laplace')
	s = floatx(sqrt(10));
elseif strcmp(ktype, 'Cauchy')
	s = floatx(sqrt(40));
end

n = size(train_x, 1); % number of training samples.
l = size(train_y, 2); % number of labels.
alpha_ep = zeros(n,l);

rs = RandStream('mt19937ar', 'Seed', 1);

cur_epoch = 0;
epro_t = 0;% for timing.
rs.reset();
for n_epoch = [1, 2]
  tic;
  fprintf('Kernel EigenPro on MNIST\n');
  alpha_ep = eigenpro_iterate(rs, train_x, train_y, alpha_ep,... 
        n_epoch-cur_epoch, mG, k, .9, "Gaussian", s);
  epro_t = epro_t + toc;
  
  train_err = evaluate(train_x, train_y, alpha_ep, "Gaussian", s, train_x) * 100;
  test_err = evaluate(test_x, test_y, alpha_ep, "Gaussian", s, train_x) * 100;
  fprintf('training error %.2f%%, testing_error %.2f%% (%d epochs, %.2f seconds)\n', ...
          train_err, test_err, n_epoch, epro_t);
end
