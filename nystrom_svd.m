tion in matlabfunction [ S, V ] = nystrom_svd(X, n, k, ktype, s)
% Compute top eigensystem of a kernel operator using nystrom method
%
% [input]
%   X: [n_samples, n_features] Subsample feature matrix
%   n : total number of samples
%   k: Number of eigendirections to be restored
%   ktype: name of kernel method
%   s: bandwidth for kernel
% [output]
%    S: top-k eigenvalues of X
%    V: top-k eigenvectors of X
   sizeDiff = n/size(X,1);
   K = kernel(X, X, s, ktype);
   W = K .* sizeDiff;
   
   % ensure K is symmetric and calculate eigensystem
   W = double((W + W.')/2);
   
   [V, S] = eigs(W, k,'LA'); % Much slower than python, 
   % will not let me use single
   S = single(max(1e-7,diag(S)));
   V = single(V(:,1:k).*sqrt(sizeDiff));
end