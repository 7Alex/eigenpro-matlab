 function [alpha] = eigenpro_iterate(rstream, X, Y, alpha, n_epoch, mG, k, tau, ktype, s)
% Train least squared regression model with feature map
%	by mini-batch EigenPro iteration.
%
% [input]
%   rstream: random number stream.
%   X: [n_example, n_raw_feature]: raw feature matrix.
%   Y: [n_example, n_label]: label matrix.
%	alpha: [n_feature, n_label]: weight matrix.
%   k: the number of eigendirections.
%   tau: damping factor.
%   ktype: name of kernel, e.g. "gaussian"
%   s: bandwidth for kernel
% [output]
%   alpha: updated weight matrix.

%Check parameters
n = size(X, 1);
if(n<100000)
    subsample_size = min(n,4000);
else
    subsample_size = 10000;
end
k = max(1,min(subsample_size, k));

% Calculate bytes that can be used. Preserve 100MB
mem_bytes = mG * (1024 ^ 3) - 100 * (1024 ^ 2);  
mem_usages = (size(X,2) + size(Y,2) + (3 * 0:subsample_size-1)) * n * 4;
mG = sum(mem_usages < mem_bytes);

pinx = randsample(rstream, n, subsample_size,false);
[max_S, beta, V, Q] = setup(X(pinx,:),k,n,mG,tau,ktype,s);

bs = max(1, beta/max_S+.5);
if(bs>n)
   bs = n;
   eta = 1.9/max_S;
elseif(bs<beta/max_S+1)
    eta = bs/beta;
else
    eta = 2*bs/(beta+(bs-1)*max_S);
end


% EigenPro iteration.
step = single(eta/bs);
for epoch = 1:n_epoch
    inx = randperm(rstream, n);
    sindex = 1;
    for eindex = int32(linspace(bs+1,n,n/bs)) 
        mbinx = inx(sindex: eindex);
        batch_x = X(mbinx, :);
        batch_y = Y(mbinx, :);
        batch_px = kernel(batch_x,X,s,ktype);
        
        g = batch_px*alpha - batch_y;
        alpha(mbinx, :) = alpha(mbinx, :) - step .* g;
        
        delta = bsxfun(@times,V,Q') * (V' * (batch_px(:,pinx)' * g));
        alpha(pinx,:) = alpha(pinx,:) + step .* delta;
        sindex = eindex;
    end
end
