function [ max_S,max_kxx, V, Q ] = setup(features,max_k, n,mG,tau, ktype, s)
% Compute preconditioner and scale factors for EigenPro iteration
%
% [input]
%   features: [n_samples, n_features] Feature matrix (normally from 
%   training data)
%   max_k : Maximum number of components to be used in EigenPro
%   iteration
%   n : total number of samples
%   mG: Maximum batch size that fits in memory
%   tau: Exponential (<=1) factor for eigenvalue ratio
%   ktype: name of kernel
%   s: bandwidth for kernel
% [output]
%    max_S: Normalized largest eigenvalue
%    max_kxx: Maximum of k(x,x) where k is the EigenPro kernel
%    V: top-k eigenvectors of features
%    Q: part of preconditioner for eigenpro step 2
    [S,V] = nystrom_svd(features,n,max_k,ktype, s);
    
    n_subsamples = size(features,1);
    
    k = sum(n ./ S < min(n_subsamples, mG))-1;
    V = V(:,1:k);
    
    scale = (S(1)./S(k+1)).^tau;
    
    Q = (1 - ((S(k+1) ./ S(1:k)) .^ tau)) ./ S(1:k);
    max_S = S(1) ./ (n.*scale);
    max_kxx = max(1 - (sum(V.^2,2) .* (n_subsamples ./ n)));
end

